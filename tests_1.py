from problema_1 import string_merge


def test_held():
    assert string_merge("hello", "world", "l") == "held"


def test_codinywhere():
    assert string_merge("coding", "anywhere", "n") == "codinywhere"


def test_jasamson():
    assert string_merge("jason", "samson", "s") == "jasamson"


def test_wondeople():
    assert string_merge("wonderful", "people", "e") == "wondeople"


def test_pere():
    assert string_merge("person", "here", "e") == "pere"


def test_apowiejouh():
    assert string_merge("apowiejfoiajsf", "iwahfeijouh", "j") == "apowiejouh"


def test_abcdefxxxyyyxyzz():
    assert string_merge("abcdefxxxyzz", "abcxxxyyyxyzz", "x") == "abcdefxxxyyyxyzz"


def test_123456789():
    assert string_merge("12345654321", "123456789", "6") == "123456789"


def test_JiOdddasdfdfsd():
    assert string_merge("JiOdIdA4", "oopopopoodddasdfdfsd", "d") == "JiOdddasdfdfsd"


def test_increople():
    assert string_merge("incredible", "people", "e") == "increople"
