from problema_2 import is_isogram


def test_Dermatoglyphics():
    assert is_isogram("Dermatoglyphics") == True


def test_isogram():
    assert is_isogram("isogram") == True


def test_isogram():
    assert is_isogram("isogram1") == False


def test_moose():
    assert is_isogram("moose") == False


def test_isIsogram():
    assert is_isogram("isIsogram") == False


def test_aba():
    assert is_isogram("aba") == False


def test_moOse():
    assert is_isogram("moOse") == False


def test_thumbscrewjapingly():
    assert is_isogram("thumbscrewjapingly") == True


def test_abcdefghijklmnopqrstuvwxyz():
    assert is_isogram("abcdefghijklmnopqrstuvwxyz") == True


def test_abcdefghijklmnopqrstuwwxyz():
    assert is_isogram("abcdefghijklmnopqrstuwwxyz") == False


def test_null():
    assert is_isogram("") == True
