def string_merge(string1, string2, letter):
    return string1[:string1.index(letter)] + string2[string2.index(letter):]


def is_isogram(string):
    return len(string) == len(set(string.lower()))
