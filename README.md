# Problema 1

Dada duas palavras e uma letra, retorne uma única palavra que seja uma combinação de ambas as palavras, mesclada no ponto em que a letra dada aparece pela primeira vez em cada palavra. A palavra retornada deve ter o início da primeira palavra e o final da segunda, com a letra divisória no meio. Você pode assumir que ambas as palavras conterão a letra divisória.

### exemplos

```
string_merge("hello", "world", "l")      ==>  "held"
string_merge("jason", "samson", "s")     ==>  "jasamson"
string_merge("coding", "anywhere", "n")  ==>  "codinywhere"
string_merge("wonderful", "people", "e") ==>  "wondeople"
```



# Problema 2

Um isograma é uma palavra que não tem letras repetidas, consecutivas ou não consecutivas. Implemente uma função que determine se uma string que contém apenas letras é um isograma. Suponha que a string vazia seja um isograma. Ignore o caso de letra.

### exemplos:

```
is_isogram("Dermatoglyphics" ) ==> True
is_isogram("aba" )             ==> False
is_isogram("moOse" )           ==> False # existe uma letra
```



## como rodar os testes:

```
pytest test_1.py
pytest test_2.py
```





> os exemplos ficarão disponíveis no link: https://gitlab.com/dunossauro/dn-dojo-2018